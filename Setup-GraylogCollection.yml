- hosts: all,!pluto.haven,!raspberrypi.dyn.haven
  tasks:

    - name: (debian) Install gnupg
      apt:
        name: gnupg
        state: present
      when: ansible_os_family == "Debian"

    - name: (debian) Check if graylog-sidecar-repository is installed
      command: dpkg-query -W graylog-sidecar-repository
      register: deb_gl2_repos
      failed_when: deb_gl2_repos.rc > 1
      changed_when: deb_gl2_repos.rc == 1
      when: ansible_os_family == "Debian"

    - name: (debian) Create temporary build directory
      tempfile:
        state: directory
        suffix: gl2tmp
      register: gl2tmp
      when: 'ansible_os_family == "Debian" and deb_gl2_repos.rc == 1'

    - name: (debian) Download graylog-sidecar-repository
      get_url: 
        url="https://packages.graylog2.org/repo/packages/graylog-sidecar-repository_1-2_all.deb"
        dest="{{ gl2tmp.path }}"
      when: 'ansible_os_family == "Debian" and deb_gl2_repos.rc == 1'
      register: deb_gl2_repos_pkg

    - name: (debian) Install graylog-sidecar-repository
      apt: deb="{{ deb_gl2_repos_pkg.dest }}"
      register: deb_gl2_repo
      when: 'ansible_os_family == "Debian" and deb_gl2_repos.rc == 1'

    - name: (debian) Setup elastic GPG key
      apt_key:
        #id: 28AB6EB572779C2AD196BE22D44C1D8DB1606F22
        url: https://artifacts.elastic.co/GPG-KEY-elasticsearch
      when: 'ansible_os_family == "Debian"'

    - name: (debian) Add elastic repo
      apt_repository:
        repo: deb https://artifacts.elastic.co/packages/7.x/apt stable main
        state: present
      register: deb_elastic_repo
      when: 'ansible_os_family == "Debian"'

    - name: Create APT preferences entry to ensure only certain packages get updated
      copy:
        src: files/graylog/etc-apt-preferences.d-graylog
        dest: /etc/apt/preferences.d/influxrepo
        owner: root
        group: root
        mode: '0644'
        backup: yes
      when: 'ansible_os_family == "Debian"'

    - name: (debian) Update APT cache
      apt:
        update_cache: yes
      when: 'ansible_os_family == "Debian" and (deb_gl2_repo.changed or deb_elastic_repo.changed)'

    - name: (debian) Install apt-transport-https,gnupg,journalbeat,filebeat,graylog-sidecar
      apt:
        name: apt-transport-https,gnupg,lsb-release,journalbeat,filebeat,graylog-sidecar,auditd
        state: latest
      when: 'ansible_os_family == "Debian"'

    - name: (rhel) Check list of packages
      package_facts:
        manager: "rpm"
      when: ansible_distribution_file_variety == "RedHat"

    - name: (rhel) Create temporary build directory
      tempfile:
        state: directory
        suffix: gl2tmp
      register: gl2tmp_rhel
      when: "ansible_distribution_file_variety == 'RedHat' and 'graylog-sidecar-repository' not in ansible_facts.packages"

    - name: (rhel) Download graylog-sidecar-repository
      get_url: 
        url="https://packages.graylog2.org/repo/packages/graylog-sidecar-repository-1-2.noarch.rpm"
        dest="{{ gl2tmp_rhel.path }}"
      register: rpm_gl2_repos_pkg
      when: "ansible_distribution_file_variety == 'RedHat' and 'graylog-sidecar-repository' not in ansible_facts.packages"

    - name: (rhel) Install graylog-sidecar-repository
      yum:
        name: "{{ rpm_gl2_repos_pkg.dest }}"
        state: present
        disable_gpg_check: yes
      when: "ansible_distribution_file_variety == 'RedHat' and 'graylog-sidecar-repository' not in ansible_facts.packages"

    - name: (rhel) Add elastic repo
      yum_repository:
        name: elastic-7.x
        description: "Elastic repository for 7.x packages"
        baseurl: "https://artifacts.elastic.co/packages/7.x/yum"
        gpgcheck: yes
        gpgkey: https://packages.elastic.co/GPG-KEY-elasticsearch
      when: ansible_distribution_file_variety == "RedHat" 

    - name: (rhel) Install journalbeat,filebeat,graylog-sidecar
      yum:
        name: journalbeat,filebeat,graylog-sidecar
        state: latest
      when: ansible_distribution_file_variety == "RedHat" 

    - name: Copy journalbeat Config
      template:
        src: files/graylog/journalbeat.yml
        dest: /etc/journalbeat/journalbeat.yml
        owner: root
        group: root
        mode: '0600'
      register: journalbeat_config

    - name: (re)Start Journalbeat
      service:
        name: journalbeat
        state: restarted
        enabled: yes
      when: journalbeat_config.changed

    - name: Copy Sidecar service
      template:
        src: files/graylog/graylog-sidecar.service
        dest: /etc/systemd/system/graylog-sidecar.service
        owner: root
        group: root
        mode: '0644'
      register: gl2_service

    - name: Copy Sidecar config
      register: gl2_config
      template:
        src: files/graylog/graylog-sidecar.yml
        dest: /etc/graylog/sidecar/sidecar.yml
        owner: root
        group: root
        mode: '0600'

    - name: Reload Systemd
      systemd:
        daemon_reload: yes
      when: gl2_service.changed

    - name: (re)Start graylog-sidecar
      service:
        name: graylog-sidecar
        state: restarted
        enabled: yes
      changed_when: false
#      when: gl2_config.changed or gl2_service.changed

    - name: Disable rsyslogd
      service:
        name: rsyslog
        state: stopped
        enabled: no
      ignore_errors: yes
